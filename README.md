#Used In
Used for Meet Magento India, Mage Titan Mexico sites

Wagento Sponsors Module for Wagento
======================

This Module for Magento� 2 allows to Add Sponsor Admin Manageable   

Facts
-----
* version: 2.0.0

Description
-----------
01/12/18

* This module is used to Add Sponsor From Admin.

Requirements
------------
* PHP >= 5.6.5

Compatibility
-------------
* Magento >= 2.1.4

Installation Instructions
-------------------------
The Wagento Sponsors module for Magento� 2 is distributed in three formats:
* Drop-In
* [Composer VCS](https://getcomposer.org/doc/05-repositories.md#using-private-repositories)

### Install Source Files ###

The following sections describe how to install the module source files,
depending on the distribution format, to your Magento� 2 instance.


Then navigate to the project root directory and run the following commands:

    composer config repositories.wagento-sponsors vcs git@bitbucket.org:wagento-global/module-sponsors.git
    composer require wagento/module-sponsors:dev-master

#### VCS ####
If you prefer to install the module using [git](https://git-scm.com/), run the
following commands in your project root directory:

    composer config repositories.wagento-sponsors vcs git@bitbucket.org:wagento-global/module-sponsors.git
    composer require wagento/module-sponsors:dev-master

### Enable Module ###
Once the source files are available, make them known to the application:

    ./bin/magento module:enable Wagento_Sponsors
    ./bin/magento setup:upgrade

Last but not least, flush cache and compile.

    ./bin/magento cache:flush
    ./bin/magento setup:di:compile

Uninstallation
--------------

The following sections describe how to uninstall the module, depending on the
distribution format, from your Magento� 2 instance.

#### Composer Git ####

To unregister the shipping module from the application, run the following command:

    ./bin/magento module:uninstall --remove-data Wagento_Sponsors

This will automatically remove source files and clean up the database.

#### Drop-In ####

To uninstall the module manually, run the following commands in your project
root directory:

    ./bin/magento module:disable Wagento_Sponsors
    rm -r app/code/Wagento/Sponsors

Features
--------------


#### Admin Settings ####

1) Add New Sponsors Type:

*  Go to Admin -> Sponsors -> Sponsors Types -> Add New Types
  
2) Add New Sponsors List:
   
*   Go to Admin -> Sponsors -> Sponsors  List -> Add New Sponsor
   
3) Store Configuration:
 
 
* Go to Admin -> Stores -> Configuration -> Wagento -> Sponsors ->
   
* General :

* Enabled = YES/No
* sponsor Headline = Add Sponsor Headline
* Placeholder Image = Add Place Holder Image
   
* Top Link:

* Enabled : YES/No
* Title: Add Title
   
* Top Menu:

* Enabled : YES/No
* Title: Add Title
* Menu Position : Add Menu Postion Like 1,2
*  Frontend URL Path : sponsors

Add Previous Sponsor


* Go to admin -> Content -> Element -> Pages -> Go to any cms Page -> Content
   Edit code add below code . Year will be dynamic you can according to your view year
  
  ```
    
  {{block class="Wagento\Sponsors\Block\YearSponsors" name="sponsor"  year="2018" template="Wagento_Sponsors::yearlist.phtml"}}
 
 ```
   

     
Developer
---------
* Pradeep Mudaliar | [Wagento](https://www.wagento.com/) | pradeep@wagento.com

License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)