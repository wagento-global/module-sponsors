<?php
/**
 * Copyright © 2019 Wagento. All rights reserved.
 */
namespace Wagento\Sponsors\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sponsors'),
                'image_width',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                     255,
                    'nullable' => true,
                    'comment' => 'Image Width'
                ]
            );
        }

        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('sponsors'),
                'year',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    'nullable' => true,
                    'comment' => 'Year'
                ]
            );
        }
        $installer->endSetup();
    }
}